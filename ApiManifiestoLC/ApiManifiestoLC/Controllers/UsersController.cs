﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Manifest.Core.Interfaces;
using Manifest.Core.Models;
using Manifest.Rules.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly IUserServices _userServices;
        public UsersController(IUserServices userServices)
        {
            _userServices = userServices;
        }


        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var user = await _userServices.GetUsers();
                return Ok(user);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost("Authentication")]
        public async Task<IActionResult> test(UserAuthentication userAuthentication)
        {
            try
            {
               await _userServices.DgaAuthentication(userAuthentication);
                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult TestEncrypt(UserAuthentication userAuthentication)
        {
            try
            {
                var encrypt  = Cryptography.Encrypt(userAuthentication.Password, userAuthentication.Username);
                return Ok(encrypt);

            }
            catch(Exception e)
            {
                return BadRequest("Error al Encriptar");
            }
        }
    }
}