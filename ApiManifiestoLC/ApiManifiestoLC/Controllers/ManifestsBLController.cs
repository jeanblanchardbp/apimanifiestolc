﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManifestsBLController : ControllerBase
    {
        private readonly ITB_IG_ManifestBLServices _manifestBL;
        public ManifestsBLController(ITB_IG_ManifestBLServices manifestBL)
        {
            _manifestBL = manifestBL;
        }

        [HttpGet]
        public async Task<IActionResult>GetManifestsBL()
        {
            try
            {
                var manifestsBL = await _manifestBL.GetManifestBLs();
                return Ok(manifestsBL);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetManifestBL( int id)
        {
            try
            {

                var manifestsBL = await _manifestBL.GetManifestBL(id);
                return Ok(manifestsBL);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateManifest([FromBody]TB_IG_ManifestBL manifestBL)
        {
            try
            {
                await _manifestBL.CreateManifestBL(manifestBL);
                return Ok("Creado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateManifest(int id,[FromBody]TB_IG_ManifestBL manifestBL)
        {
            try
            {
                await _manifestBL.UpdateManifestBL(id,manifestBL);
                return Ok("Actualizado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}