﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManifestController : ControllerBase
    {
        private readonly ITB_IG_ManifestServices _manifestService;

        public ManifestController(ITB_IG_ManifestServices manifestService)
        {
            _manifestService = manifestService;
        }

        [HttpGet]
        public async Task<IActionResult>GetManifests()
        {
            try
            {
                var manifests = await _manifestService.GetManifests();
                return Ok(manifests);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetManifest(int id)
        {
            try
            {
                var manifests = await _manifestService.GetManifest(id);
                return Ok(manifests);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateManifest(TB_IG_Manifest manifest)
        {
            try
            {
                await _manifestService.CreateManifest(manifest);
                return Ok("Creado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateManifest(int id,[FromBody]TB_IG_Manifest manifest)
        {
            try
            {
                await _manifestService.UpdateManifest(id,manifest);
                return Ok("Actualizado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}