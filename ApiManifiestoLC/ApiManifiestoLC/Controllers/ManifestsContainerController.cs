﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManifestsContainerController : ControllerBase
    {
        private readonly ITB_IG_ManifestContainerServices _manifestContainer;
        public ManifestsContainerController(ITB_IG_ManifestContainerServices manifestContainer)
        {
            _manifestContainer = manifestContainer;
        }

        [HttpGet]
        public async Task<IActionResult> GetManifestContainers()
        {
            try
            {
                var manifestC = await _manifestContainer.GetManifestContainers();
                return Ok(manifestC);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetManifestContainer(int id)
        {
            try
            {
                var manifestC = await _manifestContainer.GetManifestContainer(id);
                return Ok(manifestC);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public async Task<IActionResult> CreateManifestContainer([FromBody]TB_IG_ManifestContainer manifestC)
        {
            try
            {
                await _manifestContainer.CreateManifestContainer(manifestC);
                return Ok("Creado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public async Task<IActionResult> UpdateManifestContainer(int id, [FromBody]TB_IG_ManifestContainer manifestC)
        {
            try
            {
                await _manifestContainer.UpdateManifestContainer(id,manifestC);
                return Ok("Actualizado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}