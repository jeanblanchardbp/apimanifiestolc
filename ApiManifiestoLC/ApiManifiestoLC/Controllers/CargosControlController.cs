﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CargosControlController : ControllerBase
    {
        private readonly ITB_IG_CargoControlServices _cargoControl;

        public CargosControlController(ITB_IG_CargoControlServices cargoControl)
        {
            _cargoControl = cargoControl;
        }

        [HttpGet]
        public async Task<IActionResult> GetCargoControls()
        {
            try
            {
                var manifestCargoControl = await _cargoControl.GetCargoControls();
                return Ok(manifestCargoControl);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetCargoControl(int id)
        {
            try
            {
                var cargoC = await _cargoControl.GetCargoControl(id);
                return Ok(cargoC);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateManifestContainer([FromBody]TB_IG_CargoControl cargoControl)
        {
            try
            {
                await _cargoControl.CreateCargoControl(cargoControl);
                return Ok("Creado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateManifestContainer(int id,[FromBody]TB_IG_CargoControl cargoControl)
        {
            try
            {
                await _cargoControl.UpdateCargoControl(id,cargoControl);
                return Ok("Actualizado correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
    }