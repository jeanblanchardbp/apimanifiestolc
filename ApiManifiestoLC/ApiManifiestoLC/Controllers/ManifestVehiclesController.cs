﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manifest.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiManifiestoLC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManifestVehiclesController : ControllerBase
    {
        private readonly ITB_IG_ManifestVehicleServices _manifestVehicle;
        public ManifestVehiclesController(ITB_IG_ManifestVehicleServices manifestVehicle)
        {
            _manifestVehicle = manifestVehicle;
        }

        [HttpGet]
        public async Task<IActionResult> GetManifestVehicles()
        {
            try
            {
                var manifestV = await _manifestVehicle.GetTManifestVehicles();
                return Ok(manifestV);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }



        [HttpGet("{id}")]
        public async Task<IActionResult> GetManifestVehicle(int id)
        {
            try
            {
                var manifestV = await _manifestVehicle.GetManifestVehicle(id);
                return Ok(manifestV);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}