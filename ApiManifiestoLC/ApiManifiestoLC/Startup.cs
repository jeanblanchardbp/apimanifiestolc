using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Manifest.Data.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ApiManifiestoLC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddDbContext<ManifestDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ManifestConnection")));
            services.AddDbContext<userDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DGAWS")));

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddControllers();
            services.AddTransient<ITB_IG_ManifestServices, TB_IG_ManifestServices>();
            services.AddTransient<ITB_IG_ManifestBLServices, TB_IG_ManifestBLServices>();
            services.AddTransient<ITB_IG_CargoControlServices, TB_IG_CargoControlServices>();
            services.AddTransient<ITB_IG_ManifestContainerServices, TB_IG_ManifestContainerServices>();
            services.AddTransient<ITB_IG_ManifestVehicleServices, TB_IG_ManifestVehicleServices>();
            services.AddTransient<IUserServices, UserServices>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {   
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });                                                
        }
    }
}
