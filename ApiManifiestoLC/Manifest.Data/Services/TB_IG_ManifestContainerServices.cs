﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Data.Services
{
   public class TB_IG_ManifestContainerServices: ITB_IG_ManifestContainerServices
    {

        private readonly ManifestDbContext _manifestDbContext;
        public TB_IG_ManifestContainerServices(ManifestDbContext manifestDbContext)
        {
            _manifestDbContext = manifestDbContext;
        }

        public async Task CreateManifestContainer(TB_IG_ManifestContainer manifestContainer)
        {
            _manifestDbContext.Add(manifestContainer);
            await _manifestDbContext.SaveChangesAsync();
        }

        public Task DeleteManifestContainer(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TB_IG_ManifestContainer> GetManifestContainer(int id)
        {
            return await _manifestDbContext.ManifestContainer.FirstOrDefaultAsync(x => x.ContainerID == id);
        }

        public async Task<IEnumerable<TB_IG_ManifestContainer>> GetManifestContainers()
        {
            return await _manifestDbContext.ManifestContainer.ToListAsync();
        }

        public async Task UpdateManifestContainer(int id, TB_IG_ManifestContainer manifestContainer)
        {
            var updateManifestContainer = await GetManifestContainer(id);
            if (updateManifestContainer != null)

            {
                updateManifestContainer.ManifestID = manifestContainer.ManifestID;
                updateManifestContainer.PlacaNo = manifestContainer.PlacaNo;
                updateManifestContainer.ContainerType = manifestContainer.ContainerType;
                updateManifestContainer.PackageCode = manifestContainer.PackageCode;
                updateManifestContainer.Amount = manifestContainer.Amount;
                updateManifestContainer.GrossWeight = manifestContainer.GrossWeight;
                updateManifestContainer.NetWeight = manifestContainer.NetWeight;
                updateManifestContainer.SealNo1 = manifestContainer.SealNo1;
                updateManifestContainer.SealNo2 = manifestContainer.SealNo2;
              
                _manifestDbContext.Update(updateManifestContainer);
                await _manifestDbContext.SaveChangesAsync();
            }
        }
    }
}
