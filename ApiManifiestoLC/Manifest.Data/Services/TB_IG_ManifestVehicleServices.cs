﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Data.Services
{
   public class TB_IG_ManifestVehicleServices: ITB_IG_ManifestVehicleServices
    {
        private readonly ManifestDbContext _manifestDbContext;
        public TB_IG_ManifestVehicleServices(ManifestDbContext manifestDbContext)
        {
            _manifestDbContext = manifestDbContext;
        }

        public async Task CreateManifestVehicle(TB_IG_ManifestVehicle manifestVehicle)
        {
            _manifestDbContext.Add(manifestVehicle);
            await _manifestDbContext.SaveChangesAsync();
        }

        public Task DeleteManifestVehicle(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TB_IG_ManifestVehicle> GetManifestVehicle(int id)
        {
            return await _manifestDbContext.ManifestVehicle.FirstOrDefaultAsync(x => x.VehicleId == id);
        }
                    

        public async Task<IEnumerable<TB_IG_ManifestVehicle>> GetTManifestVehicles()
        {
            return await _manifestDbContext.ManifestVehicle.ToListAsync();
        }

        public async Task UpdateManifestVehicle(int id, TB_IG_ManifestVehicle manifestVehicle)
        {
            var updateManifestVehicle = await GetManifestVehicle(id);
            if (updateManifestVehicle != null)

            {
                updateManifestVehicle.ManifestID = manifestVehicle.ManifestID;
                updateManifestVehicle.ManifestBLID = manifestVehicle.ManifestBLID;
                updateManifestVehicle.BLNo = manifestVehicle.BLNo;
                updateManifestVehicle.Vehicletype = manifestVehicle.Vehicletype;
                updateManifestVehicle.ChassisNo = manifestVehicle.ChassisNo;
                updateManifestVehicle.GrossWeight = manifestVehicle.GrossWeight;
                updateManifestVehicle.CarYear = manifestVehicle.CarYear;
                updateManifestVehicle.BrandCode = manifestVehicle.BrandCode;
                updateManifestVehicle.ModelCode = manifestVehicle.ModelCode;
                updateManifestVehicle.Displacement = manifestVehicle.Displacement;
                updateManifestVehicle.Color = manifestVehicle.Color;
                updateManifestVehicle.Options = manifestVehicle.Options;
                updateManifestVehicle.BrandName = manifestVehicle.BrandName;
                updateManifestVehicle.ModelName = manifestVehicle.ModelName;

                _manifestDbContext.Update(updateManifestVehicle);
                await _manifestDbContext.SaveChangesAsync();
            }
        }
    }
}
