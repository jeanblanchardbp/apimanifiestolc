﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Data.Services
{
   public class TB_IG_ManifestBLServices: ITB_IG_ManifestBLServices
    {
        private readonly ManifestDbContext _manifestDbContext;
        public TB_IG_ManifestBLServices(ManifestDbContext manifestDbContext)
        {
            _manifestDbContext = manifestDbContext;
        }

        public async Task CreateManifestBL(TB_IG_ManifestBL manifestBL)
        {
            _manifestDbContext.Add(manifestBL);
            await _manifestDbContext.SaveChangesAsync();
        }

        public Task DeleteManifestBL(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TB_IG_ManifestBL> GetManifestBL(int id)
        {
            return await _manifestDbContext.ManifestBL.FirstOrDefaultAsync(x => x.ManifestBLID == id);
        }

        public async Task<IEnumerable<TB_IG_ManifestBL>> GetManifestBLs()
        {
            return await _manifestDbContext.ManifestBL.ToListAsync();
        }

        public async Task UpdateManifestBL(int id, TB_IG_ManifestBL manifestBL)
        {

            var updateManifestBL = await GetManifestBL(id);
            if (updateManifestBL != null)

            {
                updateManifestBL.ManifestID = manifestBL.ManifestID;
                updateManifestBL.ManifestNo = manifestBL.ManifestNo;
                updateManifestBL.BLNo = manifestBL.BLNo;
                updateManifestBL.HouseBLNo = manifestBL.HouseBLNo;
                updateManifestBL.CargoControlID = manifestBL.CargoControlID;
                updateManifestBL.CargoControlNo = manifestBL.CargoControlNo;
                updateManifestBL.BLType = manifestBL.BLType;
                updateManifestBL.TransitType = manifestBL.TransitType;
                updateManifestBL.ConsignorCode = manifestBL.ConsignorCode;
                updateManifestBL.ConsignorTel = manifestBL.ConsignorTel; 
                updateManifestBL.NotifyCode = manifestBL.NotifyCode;
                updateManifestBL.NotifyName = manifestBL.NotifyName; 
                updateManifestBL.NotifyTel = manifestBL.NotifyTel;
                updateManifestBL.NotifyAddress = manifestBL.NotifyAddress; 
                updateManifestBL.ExpressType = manifestBL.ExpressType;
                updateManifestBL.TransitPortCode = manifestBL.TransitPortCode; 
                updateManifestBL.LoadingPortCode = manifestBL.LoadingPortCode;
                updateManifestBL.GoodsName = manifestBL.GoodsName;
                updateManifestBL.PackageUnitCode = manifestBL.PackageUnitCode;
                updateManifestBL.PackageQty = manifestBL.PackageQty;
                updateManifestBL.GrossWeight = manifestBL.GrossWeight;
                updateManifestBL.Value = manifestBL.Value;
                updateManifestBL.ValueType = manifestBL.ValueType;
                updateManifestBL.FlightCharge = manifestBL.FlightCharge;
                updateManifestBL.Volume = manifestBL.Volume;
                updateManifestBL.HouseBLSubmitYN = manifestBL.HouseBLSubmitYN;
                updateManifestBL.GrossWeightUnitCode = manifestBL.GrossWeightUnitCode;
                updateManifestBL.PackingType = manifestBL.PackingType;
                updateManifestBL.ConsolidatedBLID = manifestBL.ConsolidatedBLID;
                updateManifestBL.RefCargoControlNo = manifestBL.RefCargoControlNo;
                updateManifestBL.ConsignorName = manifestBL.ConsignorName;
                updateManifestBL.ConsignorAddress = manifestBL.ConsignorAddress;
                updateManifestBL.ConsigneeName = manifestBL.ConsigneeName;
                updateManifestBL.ConsigneeAddress = manifestBL.ConsigneeAddress;

                _manifestDbContext.Update(updateManifestBL);
                await _manifestDbContext.SaveChangesAsync();
            }
        }
    }
}
