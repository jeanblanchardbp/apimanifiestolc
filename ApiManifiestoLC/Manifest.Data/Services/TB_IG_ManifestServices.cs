﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Data.Services
{
   public class TB_IG_ManifestServices: ITB_IG_ManifestServices
    {
        private readonly ManifestDbContext _manifestDbContext;
        public TB_IG_ManifestServices(ManifestDbContext manifestDbContext)
        {
            _manifestDbContext = manifestDbContext;
        }

        public async Task CreateManifest(TB_IG_Manifest Manifest)
        {
            _manifestDbContext.Add(Manifest);
            await _manifestDbContext.SaveChangesAsync();
        }

        public Task DeleteMManifest(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TB_IG_Manifest> GetManifest(int id)
        {
            return await _manifestDbContext.Manifest.FirstOrDefaultAsync(manifest => manifest.ManifestID == id);
        }

        public async Task<IEnumerable<TB_IG_Manifest>> GetManifests()
        {
            return await _manifestDbContext.Manifest.ToListAsync();
        }

        public async Task UpdateManifest(int id, TB_IG_Manifest Manifest)
        {
            var updateManifest = await GetManifest(id);

            if (updateManifest != null)
            {
                updateManifest.ManifestNo = Manifest.ManifestNo;
                updateManifest.AreaCode = Manifest.AreaCode;
                updateManifest.TransportType = Manifest.TransportType;
                updateManifest.ManifestType = Manifest.ManifestType;
                updateManifest.InOutType = Manifest.InOutType;
                updateManifest.CompanyCode = Manifest.CompanyCode;
                updateManifest.VesselCode = Manifest.VesselCode;
                updateManifest.VoyageNo = Manifest.VoyageNo;
                updateManifest.EmptyYN = Manifest.EmptyYN;
                updateManifest.LoadingLocationCode = Manifest.LoadingLocationCode;
                updateManifest.UnloadingLocationCode = Manifest.UnloadingLocationCode;
                updateManifest.ViaEntrance = Manifest.ViaEntrance;
                updateManifest.DepartureDate = Manifest.DepartureDate;
                updateManifest.ActualDepartureDate = Manifest.ActualDepartureDate;
                updateManifest.ArrivalDate = Manifest.ArrivalDate;
                updateManifest.ActualArrivalDate = Manifest.ActualArrivalDate;
                updateManifest.ApplicantCode = Manifest.ApplicantCode;
                updateManifest.StatusCode = Manifest.StatusCode;
                updateManifest.Version = Manifest.Version;
                updateManifest.RegisterCode = Manifest.RegisterCode;
                updateManifest.RegisterDate = Manifest.RegisterDate;
                updateManifest.Updatedate = Manifest.Updatedate;
                updateManifest.Effectivedate = Manifest.Effectivedate;
                updateManifest.Saveflag = Manifest.Saveflag;
                updateManifest.BizCompanyCode = Manifest.BizCompanyCode;
                updateManifest.CountryCode = Manifest.CountryCode;
                updateManifest.RegisterLocation = Manifest.RegisterLocation;
                updateManifest.UpdaterCode = Manifest.UpdaterCode;
                updateManifest.UpdaterLocation = Manifest.UpdaterLocation;
                updateManifest.PathType = Manifest.PathType;
                updateManifest.ConsistencyCode = Manifest.ConsistencyCode;

                _manifestDbContext.Update(updateManifest);
                await _manifestDbContext.SaveChangesAsync();
            }
        }
    }
}
