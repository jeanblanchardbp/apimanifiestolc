﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Core.Models;
using Manifest.Data.Context;
using Manifest.Rules.Helpers;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;


namespace Manifest.Data.Services
{

    public class UserServices : IUserServices
    {
        private readonly userDbContext _userContext;
        private string isPasswordValid;

        public UserServices(userDbContext userContext)
        {
            _userContext = userContext;
        }

        public async Task DgaAuthentication(UserAuthentication auth)
        {
            var authUser = await _userContext.Users.FirstOrDefaultAsync(user => user.Username == auth.Username);
            if (authUser !=null)
            {
               var dcrypt = Cryptography.Encrypt(auth.Password, auth.Username);
                if (dcrypt.Equals(authUser.Password))
                {
                    isPasswordValid = "si";
                }
                else
                {
                    isPasswordValid = "no";
                }

                //byte[] passwordHash = System.Text.Encoding.UTF8.GetBytes(authUser.Password);
                //string password = Convert.ToBase64String(passwordHash);
            }

        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userContext.Users.ToListAsync();
        }
    }
}
