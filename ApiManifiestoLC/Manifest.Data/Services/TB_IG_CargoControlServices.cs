﻿using Manifest.Core.Entities;
using Manifest.Core.Interfaces;
using Manifest.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Data.Services
{
   public class TB_IG_CargoControlServices: ITB_IG_CargoControlServices
    {
        private readonly ManifestDbContext _manifestDbContext;

        public TB_IG_CargoControlServices(ManifestDbContext manifestDbContext)
        {
            _manifestDbContext = manifestDbContext;
        }

        public async Task CreateCargoControl(TB_IG_CargoControl cargoControl)
        {
            _manifestDbContext.Add(cargoControl);
            await _manifestDbContext.SaveChangesAsync();
        }

        public Task DeleteCargoControl(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TB_IG_CargoControl> GetCargoControl(int id)
        {
            return await _manifestDbContext.CargoControl.FirstOrDefaultAsync(x => x.CargoControlID == id);
        }

        public async Task<IEnumerable<TB_IG_CargoControl>> GetCargoControls()
        {
            return await _manifestDbContext.CargoControl.ToListAsync();
        }

        public async Task UpdateCargoControl(int id, TB_IG_CargoControl cargoControl)
        {
            var updateCargoControl = await GetCargoControl(id);
            if (updateCargoControl != null)

            {
                updateCargoControl.CargoControlNo = cargoControl.CargoControlNo;
                updateCargoControl.ManifestID = cargoControl.ManifestID;
                updateCargoControl.ManifestNo = cargoControl.ManifestNo;
                updateCargoControl.BLNo = cargoControl.BLNo;
                updateCargoControl.HouseBLNo = cargoControl.HouseBLNo;
                updateCargoControl.UnloadingYN = cargoControl.UnloadingYN;
                updateCargoControl.EntryYN = cargoControl.ReleaseYN;
                updateCargoControl.ReleaseYN = cargoControl.ReleaseYN;
                updateCargoControl.LoadingYN = cargoControl.LoadingYN;
                updateCargoControl.OverDueNotifyYN = cargoControl.OverDueNotifyYN;
                updateCargoControl.ParentCargoControlNo = cargoControl.ParentCargoControlNo;
                updateCargoControl.RegisterDate = cargoControl.RegisterDate;
                updateCargoControl.UpdateDate = cargoControl.UpdateDate;
                updateCargoControl.EffectiveDate = cargoControl.EffectiveDate;
                      
                _manifestDbContext.Update(updateCargoControl);
                await _manifestDbContext.SaveChangesAsync();
            }
        }
    }
}
