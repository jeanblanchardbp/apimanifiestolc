﻿using Manifest.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Data.Context
{
   public class userDbContext:DbContext
    {
        public userDbContext(DbContextOptions<userDbContext> options): base (options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("Users");
        }

        public DbSet <User> Users { get; set; }

    }
}
