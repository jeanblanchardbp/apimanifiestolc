﻿
using Manifest.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Data.Context
{
   public class ManifestDbContext: DbContext
    {
        public ManifestDbContext(DbContextOptions<ManifestDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TB_IG_CargoControl>().ToTable("TB_IG_CargoControl");
            modelBuilder.Entity<TB_IG_Manifest>().ToTable("TB_IG_Manifest");
            modelBuilder.Entity<TB_IG_ManifestBL>().ToTable("TB_IG_ManifestBL");
            modelBuilder.Entity<TB_IG_ManifestContainer>().ToTable("TB_IG_ManifestContainer");
            modelBuilder.Entity<TB_IG_ManifestVehicle>().ToTable("TB_IG_ManifestVehicle");
        }

        public DbSet<TB_IG_CargoControl> CargoControl { get; set; }
        public DbSet<TB_IG_Manifest> Manifest { get; set; }
        public DbSet<TB_IG_ManifestBL> ManifestBL { get; set; }
        public DbSet<TB_IG_ManifestContainer> ManifestContainer { get; set; }
        public DbSet<TB_IG_ManifestVehicle> ManifestVehicle { get; set; }
    }
}
