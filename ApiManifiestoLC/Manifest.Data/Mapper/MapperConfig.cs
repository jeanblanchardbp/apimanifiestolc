﻿using AutoMapper;
using Manifest.Core.Dto;
using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Data.Mapper
{
   public class MapperConfig: Profile
    {
        public MapperConfig()
        {
            CreateMap<TB_IG_Manifest, TB_IG_ManifestDto>().ReverseMap();
            CreateMap<TB_IG_ManifestBL, TB_IG_ManifestBLDto>().ReverseMap();
            CreateMap<TB_IG_CargoControl, TB_IG_CargoControlDto>().ReverseMap();
            CreateMap<TB_IG_ManifestContainer, TB_IG_ManifestContainerDto>().ReverseMap();
            CreateMap<TB_IG_ManifestVehicle, TB_IG_ManifestVehicleDto>().ReverseMap();
        }
    }

   
}
