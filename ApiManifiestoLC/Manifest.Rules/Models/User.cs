﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Rules.Models
{
    public class User
    {
        /// <summary>
        /// this is the user Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// this is the user code
        /// </summary>
        public string UserCode { get; set; }

        /// <summary>
        /// user name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// This is a password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// this a company code
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// Is active
        /// </summary>
        [Display(Name = "Activo")]
        public bool IsActive { get; set; }
    }
}
