﻿using Manifest.Rules.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Rules.Models.Authentication
{
    public sealed class AuthenticationModel
    {

        public string Username { get; }
        public string Password { get; }

        public AuthenticationModel(string username, string password)
        {
            Username = username;
            Password = Cryptography.Encrypt(password, username);
        }
    }
}
