﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Rules.Helpers
{
   public static class StringExtenstions
    {
        public static string NormalizeName(this string text)
        {
            return text.Replace("\"", "");
        }
    }
}
