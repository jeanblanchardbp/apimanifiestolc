﻿using Manifest.Core.Entities;
using Manifest.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public  interface IUserServices
    {
        public Task<IEnumerable<User>> GetUsers();

        public Task DgaAuthentication(UserAuthentication auth);

    }
}
