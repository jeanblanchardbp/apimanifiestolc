﻿using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public interface ITB_IG_ManifestContainerServices
    {
        public Task<IEnumerable<TB_IG_ManifestContainer>> GetManifestContainers();
        public Task<TB_IG_ManifestContainer> GetManifestContainer(int id);
        public Task CreateManifestContainer(TB_IG_ManifestContainer manifestContainer);
        public Task UpdateManifestContainer(int id, TB_IG_ManifestContainer manifestContainer);
        public Task DeleteManifestContainer(int id);
    }
}
