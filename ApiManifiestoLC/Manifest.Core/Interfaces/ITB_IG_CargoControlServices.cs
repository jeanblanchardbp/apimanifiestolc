﻿using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public interface ITB_IG_CargoControlServices
    {
        public Task<IEnumerable<TB_IG_CargoControl>> GetCargoControls();
        public Task<TB_IG_CargoControl> GetCargoControl(int id);
        public Task CreateCargoControl(TB_IG_CargoControl cargoControl);
        public Task UpdateCargoControl(int id, TB_IG_CargoControl cargoControl);
        public Task DeleteCargoControl(int id);
    }
}
