﻿using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public interface ITB_IG_ManifestBLServices
    {
        public Task<IEnumerable<TB_IG_ManifestBL>> GetManifestBLs();
        public Task<TB_IG_ManifestBL> GetManifestBL(int id);
        public Task CreateManifestBL(TB_IG_ManifestBL manifestBL);
        public Task UpdateManifestBL(int id, TB_IG_ManifestBL manifestBL);
        public Task DeleteManifestBL(int id);
    }
}
