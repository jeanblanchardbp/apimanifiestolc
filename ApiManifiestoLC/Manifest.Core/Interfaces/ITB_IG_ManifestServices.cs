﻿using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public interface ITB_IG_ManifestServices
    {
        public Task<IEnumerable<TB_IG_Manifest>> GetManifests();
        public Task<TB_IG_Manifest> GetManifest(int id);
        public Task CreateManifest(TB_IG_Manifest Manifest);
        public Task UpdateManifest(int id, TB_IG_Manifest Manifest);
        public Task DeleteMManifest(int id);
    }
}
