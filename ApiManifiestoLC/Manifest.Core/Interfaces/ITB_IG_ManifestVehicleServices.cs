﻿using Manifest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manifest.Core.Interfaces
{
   public interface ITB_IG_ManifestVehicleServices
    {
        public Task<IEnumerable<TB_IG_ManifestVehicle>> GetTManifestVehicles();
        public Task<TB_IG_ManifestVehicle> GetManifestVehicle(int id);
        public Task CreateManifestVehicle(TB_IG_ManifestVehicle ManifestVehicle);
        public Task UpdateManifestVehicle(int id, TB_IG_ManifestVehicle ManifestVehicle);
        public Task DeleteManifestVehicle(int id);
    }
}
