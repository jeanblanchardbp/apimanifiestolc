﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Core.Entities
{
   public class TB_IG_ManifestBL
    {
        [Key]
        public Int64? ManifestBLID { get; set; }
        public Int64? ManifestID { get; set; }
        public string ManifestNo { get; set; }
        public string BLNo { get; set; }
        public string HouseBLNo { get; set; }
        public Int64? CargoControlID { get; set; }
        public string CargoControlNo { get; set; }
        public string BLType { get; set; }
        public string TransitType { get; set; }
        public string ConsignorCode { get; set; }
        public string ConsignorTel { get; set; }
        public string NotifyCode { get; set; }
        public string NotifyName { get; set; }
        public string NotifyTel { get; set; }
        public string NotifyAddress { get; set; }
        public string ExpressType { get; set; }
        public string TransitPortCode { get; set; }
        public string LoadingPortCode { get; set; }
        public string GoodsName { get; set; }
        public string PackageUnitCode { get; set; }
        public decimal? PackageQty { get; set; }
        public decimal? GrossWeight { get; set; }
        public decimal? Value { get; set; }
        public string ValueType { get; set; }
        public decimal? FlightCharge { get; set; }
        public decimal? Volume { get; set; }
        public string HouseBLSubmitYN { get; set; }
        public string GrossWeightUnitCode { get; set; }
        public string PackingType { get; set; }
        public Int64? ConsolidatedBLID { get; set; }
        public string RefCargoControlNo { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignorAddress { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneeAddress { get; set; }
    }
}
