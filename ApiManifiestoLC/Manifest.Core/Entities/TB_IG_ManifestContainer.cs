﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Core.Entities
{
   public class TB_IG_ManifestContainer
    {
        [Key]
        public Int64? ContainerID { get; set; }
        public Int64? ManifestID { get; set; }
        public string PlacaNo { get; set; }
        public string ContainerType { get; set; }
        public string PackageCode { get; set; }
        public int? Amount { get; set; }
        public decimal GrossWeight { get; set; }
        public decimal NetWeight { get; set; }
        public string SealNo1 { get; set; }
        public string SealNo2 { get; set; }
    }
}
