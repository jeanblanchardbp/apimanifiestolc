﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Core.Entities
{
   public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Username { get; set; }
        public string UserCode { get; set; }
        public string Password { get; set; }
        public string CompanyCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime? Created { get; set; }
        
    }
}
