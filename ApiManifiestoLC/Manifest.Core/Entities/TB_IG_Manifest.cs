﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Core.Entities
{
   public class TB_IG_Manifest
    {
        [Key]
        public Int64? ManifestID { get; set; }
        public string ManifestNo { get; set; }
        public string AreaCode { get; set; }
        public string TransportType { get; set; }
        public string ManifestType { get; set; }
        public string InOutType { get; set; }
        public string CompanyCode { get; set; }
        public string VesselCode { get; set; }
        public string VoyageNo { get; set; }
        public char? EmptyYN { get; set; }
        public string LoadingLocationCode { get; set; }
        public string UnloadingLocationCode { get; set; }
        public string ViaEntrance { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ActualDepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? ActualArrivalDate { get; set; }
        public string ApplicantCode { get; set; }
        public string StatusCode { get; set; }
        public decimal? Version { get; set; }
        public string RegisterCode { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? Updatedate { get; set; }
        public DateTime? Effectivedate { get; set; }
        public char? Saveflag { get; set; }
        public string BizCompanyCode { get; set; }
        public string CountryCode { get; set; }
        public string RegisterLocation { get; set; }
        public string UpdaterCode { get; set; }
        public string UpdaterLocation { get; set; }
        public string PathType { get; set; }
        public string ConsistencyCode { get; set; }
    }
}
