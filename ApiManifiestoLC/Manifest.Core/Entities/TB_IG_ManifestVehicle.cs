﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manifest.Core.Entities
{
   public class TB_IG_ManifestVehicle
    {
        [Key]
        public Int64 VehicleId { get; set; }
        public Int64 ManifestID { get; set; }
        public Int64 ManifestBLID { get; set; }
        public string BLNo { get; set; }
        public string Vehicletype { get; set; }
        public string ChassisNo { get; set; }
        public decimal GrossWeight { get; set; }
        public string CarYear { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public decimal Displacement { get; set; }
        public string Color { get; set; }
        public string Options { get; set; }
        public string BrandName { get; set; }
        public string ModelName { get; set; }
    }
}
