﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Core.Dto
{
   public class TB_IG_CargoControlDto
    {
        public string CargoControlNo { get; set; }
        public Int64? ManifestID { get; set; }
        public string ManifestNo { get; set; }
        public string BLNo { get; set; }
        public string HouseBLNo { get; set; }
        public char? UnloadingYN { get; set; }
        public char? EntryYN { get; set; }
        public char? ReleaseYN { get; set; }
        public char? LoadingYN { get; set; }
        public char? OverDueNotifyYN { get; set; }
        public string ParentCargoControlNo { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }
}
