﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manifest.Core.Dto
{
   public class TB_IG_ManifestContainerDto
    {
        public Int64? ManifestID { get; set; }
        public string PlacaNo { get; set; }
        public string ContainerType { get; set; }
        public string PackageCode { get; set; }
        public int? Amount { get; set; }
        public float GrossWeight { get; set; }
        public float NetWeight { get; set; }
        public string SealNo1 { get; set; }
        public string SealNo2 { get; set; }
    }
}
